﻿/// ETML
/// Auteur:         Sébastien Tille
/// Date:           05 octobre 2023
/// Description:    Gestionnaire de tâches permettant:
///                   1. Insértion de tâches;
///                   2. Lister les tâches;
///                   3. Terminer un tâche;
///                   4. Quitter.

using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager
{
    internal class Program
    {
        private const int ITEMS_PER_TASK = 2;
        private const int MAX_TASKS = 10;
        private const string TASK_IN_PROGRESS = "Tâche en cours";
        private const string TASK_COMPLETED = "Tâche terminée";

        private static int _tasksCount;
        private static string[,] _tasks;
        private static string[] _menuItems = new string[]
        {
            "[1] Insérer une têche.",
            "[2] Lister les tâches",
            "[3] Terminer une tâche",
            "[4] Quitter"
        };

        static void Main(string[] args)
        {
            _tasks = new string[ITEMS_PER_TASK, MAX_TASKS];
            _tasksCount = 0;
            
            Console.WriteLine(
                "==========================\n" +
                "  GESTIONNAIRE DE TACHES  \n" +
                "==========================\n" +
                "\n" +
                "Ceci est un gestionnaire de tâches réalisé en C#." +
                "\n" +
                "\n" +
                "Appuyez [enter] pour continuer.");
            //Console.ReadLine();

            do
            {
                var v = ShowMenu();
                PerformChoice(v);
            }
            while (!Console.ReadLine().Equals(_menuItems.Length));
        }

        /// <summary>
        /// Permet d'ajouter une tâche, si l'espace le permet.
        /// </summary>
        private static void AddTask()
        {
            Console.Clear();
            Console.WriteLine(
                "=================\n" +
                "Ajoutez une tâche\n" +
                "=================\n" +
                "\n");

            if (_tasksCount >= MAX_TASKS)
            {
                Console.WriteLine("Nombre maximal de tâches atteint.");
            }
            else
            {
                Console.WriteLine("Entrez un descriptif pour la tâche:");
                var str = Console.ReadLine();
                _tasks[_tasksCount, 0] = str;
                _tasks[_tasksCount, 1] = TASK_IN_PROGRESS;
                ++_tasksCount;
            }
        }

        /// <summary>
        /// Affiche toutes les tâches et leur états.
        /// </summary>
        private static void DisplayTaskList()
        {
            Console.WriteLine(
                "================\n" +
                "Liste des tâches\n" +
                "================\n" +
                "\n");
            for (int i = 0; i < _tasksCount; ++i)
            {
                Console.WriteLine($"{i + 1}. {_tasks[i,0]} {_tasks[i,1]}");
            }
        }

        /// <summary>
        /// Permet de marquer une tâche comme terminée.
        /// </summary>
        private static void CompleteTask()
        {
            var choice = -1;

            Console.Clear();
            Console.WriteLine(
                "==================\n" +
                "Terminer une tâche\n" +
                "==================\n" +
                "\n");
            DisplayTaskList();

            do
            {
                Console.Write("Quelle tâche terminer?");
                choice = Convert.ToInt16(Console.ReadLine());
            }
            while (choice < 1 || _tasksCount < choice);

            _tasks[choice - 1, 1] = TASK_COMPLETED;
            DisplayTaskList();
        }

        /// <summary>
        /// Effectue le choix de l'utilisateur passé en paramètre.
        /// </summary>
        /// <param name="i">le choix de l'utilisateur</param>
        private static void PerformChoice(int i)
        {
            switch (i)
            {
                case 1:
                    AddTask();
                    break;
                case 2:
                    DisplayTaskList();
                    break;
                case 3:
                    CompleteTask();
                    break;
                case 4:
                    Environment.Exit(0);
                    break;
                default:
                    ShowMenu();
                    break;
            }
        }

        /// <summary>
        /// Affiche le menu.
        /// </summary>
        /// <returns>le choix de l'utilisateur</returns>
        private static int ShowMenu()
        {
            int choice = -1;

            // Effacer la console et afficher les objets du menu.
            Console.Clear();
            Console.WriteLine("Que voulez-vous faire?\n");
            foreach (var m in _menuItems) Console.WriteLine(m);

            // Demander son choix à l'utilisateur.
            do
            {
                Console.Write("\nVotre choix: ");
                choice = Convert.ToInt16(Console.ReadLine());
            }
            while (choice < 1 || _menuItems.Length < choice);

            return choice;
        }
    }
}
